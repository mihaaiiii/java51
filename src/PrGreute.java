import java.util.Scanner;

public class PrGreute {
    public static void main(String[] args) {
        // suma tutor patratelor perfecte până la nr n citit de la tastatura
        Scanner keyboard = new Scanner(System.in);

        int n = keyboard.nextInt();
        int i = 1;
        int suma = 0;
        while (i * i <= n) {
            suma = suma + (i * i);
            i++;
        }
        System.out.println("Suma pătratelor perfecte mai mici decât " + n + " = " + suma);

        // ultima cifră a lui n
        n = keyboard.nextInt();
        System.out.println("Ultima cifra a lui " + n + " este " + (n % 10)); // 23/10 = 2 rest 3, 45/10 = 4 rest 5
        System.out.println("Ultimele două cifre a lui " + n + " sunt " + (n % 100)); // 23/10 = 2 rest 3, 45/10 = 4 rest 5
        System.out.println("Numărul " + n + " fara ultiam cifră e " + n / 10); // 7897/10 = 789 ("taie" ultima cifră)

        // afișeaza toate cifrele lui n
        // 45678 -> afisez 8 și scot ultima cifra din numar
        // 4567  -> afisez 7 și scot ultima cifra din numar
        // 456
        // 45
        // 4
        // 0
        n = keyboard.nextInt();
        while (n > 0) {
            System.out.print(n % 10 + " ");
            n = n / 10;
        }

        // calculați suma tutor cifrelor lui n
        // câte cifre are n
        // câte cifre pare are n
        // câte cifre impare are n


        // toti divizorii unui număr sunt mai mici decât el
        // 12 are ca divizori : 1, 2, 3, 4, 6, 12
        // afisati toti divizorii lui n citit de la tastatura
        n = keyboard.nextInt();
        for(i = 1; i <= n; i++) {
            if(n%i == 0) {
                System.out.println(i + " ");
            }
        }

        // suma divizorilor lui n
        // cati dvizori are n
        // cati divizori propri are n (divizor propriu = diferit de 1 si el insusi)
        // 12 are 6 divizori
        // 12 are 4 divizori propri

    }
}
