package oop;

import oop.school.Person;

public class Book {

    private String title;
    private String author;
    private Integer bookId;
    private double price;

    // constructorul default pe care îl crează Java dacă niciun alt constructor nu există
//    public Book () {
//
//    }

    //method signature: numele metodei, numarul parametrilor, tipul parametrilor

    public Book(String pTtile) {
        title = pTtile;
    }

    public Book(String title, String author) {
        this.title = title;
        this.author = author;
    }

    public Book(Integer id) {
        bookId = id;
    }

    public void displayBookInfo() {
        String title = "Noul titlu Local";
        System.out.println("Titlul local: " + title);
        System.out.println(this.title + " de " + author);
    }

    public void displayBookInfoWithId() {
        System.out.println(this.title + " de " + this.author + " cu id-ul " + this.bookId);
    }

    public static void main(String[] args) {
        Book b1 = new Book("Batranul si marea");
        b1.author = "Hemingway";
        b1.bookId = 13;


        // castin and data loss
        b1.price = 123.3;
        System.out.println((int) b1.price);

        Integer nr = Integer.MAX_VALUE;
        nr++; // overflow
        System.out.println(nr);

        Book b2 = new Book("Batranul si marea");
        b2.author = "Hemingway";
        b2.bookId = 13;


        int n1 = 15;
        int n2 = 15;

        if (n1 == n2) {
            System.out.println("doua primititve sunt egale cu ==");
        }

        Integer nn1 = 15;
        Integer nn2 = 15;

        if (nn1.equals(nn2)) {
            System.out.println("Sunt egale doua obiecte cu equals");
        }

        if (b1.equals(b2)) {
            System.out.println("b1 equals b2");
        } else {
            System.out.println("b1 != b2");
        }

        String s1 = "mama";
        String s2 = "mama";
        System.out.println(s1 == s2);

    }

    public boolean equals(Book obj) {
        if (this.title.equals(obj.title)
                && this.author.equals(obj.author)
                && this.bookId.equals(obj.bookId)) {
            return true;
        } else {
            return false;
        }
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public Integer getBookId() {
        return bookId;
    }
}
