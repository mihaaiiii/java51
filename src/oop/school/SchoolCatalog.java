package oop.school;

import java.util.Scanner;

public class SchoolCatalog {

    public static void main(String[] args) {
        Student s0 = new Student("Ana", 12, 7.6);
        System.out.println(s0.introduceYourself());
        System.out.println(s0.getPragBursa());


        Student s1 = new Student("Maria", 10, 9.4);
        System.out.println(s1.introduceYourself());
        System.out.println(s1.getPragBursa());

        Student.setPragBursa(7);
        System.out.println(Student.getPragBursa());

        System.out.println(s1.getPragBursa());


    }


}

