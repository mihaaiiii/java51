package oop.school;

import java.io.OptionalDataException;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoField;

public class Person {


    // GYYMMdd
    // G = 1 sau 2 => persoana e născută în 1900+
    // G = 5 sau 6 => persoana e născută în 2000+
    public static final int CNP_POZITIE_GEN = 0;
    // cnp-ul pentru M începe cu 1 sau 5
    private static final char[] CNP_BARBATI = {'1', '5'};
    // cnp-ul pentru F începe cu 2 sau 6
    public static final char[] CNP_FEMEI = {'2', '6', '8'};

    private final String cnp;
    private final LocalDate dataNasterii;

    private String nume;
    private String prenume;

    private double salariul;

    private static double salariulMinim;

    // un constructor public

    public Person(String cnp, String nume, String prenume, double salariul) {
        this.cnp = cnp;
        this.nume = nume;
        this.prenume = prenume;
        this.salariul = salariul;

        String sYear = "" + cnp.charAt(1) + cnp.charAt(2);
        int year = Integer.valueOf(sYear); // 56
        if (cnp.charAt(0) == '1' || cnp.charAt(0) == '2') {
            year = year + 1900; // 1956
        } else {
            year = year + 2000;
        }

        String sMonth = "" + cnp.charAt(3) + cnp.charAt(4);
        int month = Integer.valueOf(sMonth);

        String sDay = "" + cnp.charAt(5) + cnp.charAt(6);
        int day = Integer.valueOf(sDay);

        dataNasterii = LocalDate.of(year, month, day);
    }


    public String getCnp() {
        return cnp;
    }

    // exemplu în care nu avem nevoie de setter
//    public void setCnp(String noulCnp) {
//        cnp = noulCnp;
//    }

    public String getNume() {
        return nume;
    }

    public void setNume(String noulNume) {
        nume = noulNume;
    }

    public String getPrenume() {
        return prenume;
    }

    public void setPrenume(String noulPrenume) {
        prenume = noulPrenume;
    }

    public double getSalariul() {
        return salariul;
    }

    public void setSalariul(double noulSalariu) {
        if (noulSalariu < salariulMinim) {
            System.out.println("Salariul nu poate fi mai mic decât salariul minim pe economie," +
                    " care actualmente este: " + salariulMinim);
        } else {
            salariul = noulSalariu;
        }
    }

    public static double getSalariulMinim() {
        return salariulMinim;
    }

    public static void setSalariulMinim(double noulSalariuMinim) {
        if (noulSalariuMinim < salariulMinim) {
            System.out.println("Nu putem scădea salariul minim pe economie de la " +
                    salariulMinim + " la " + noulSalariuMinim);
        } else {
            salariulMinim = noulSalariuMinim;
        }
    }

    public LocalDate getDataNasterii() {
        return dataNasterii;
    }

    public boolean isMale() {
        char gender = cnp.charAt(0);
        if (gender == '1' || gender == '5') {
            return true;
        } else {
            return false;
        }
    }

    public boolean isFemale() {
        char gender = cnp.charAt(0);
        if (gender == '2' || gender == '6') {
            return true;
        } else {
            return false;
        }
    }

    public boolean isFemale2() {
        // returneaza caracterul de pe pozitia CNP_POZITIE_GEN (character At position CNP_POZITIE_GEN)
        char gender = cnp.charAt(CNP_POZITIE_GEN); // stocheaza primul caracter din field-ul cnp
        if (gender == CNP_FEMEI[0] || gender == CNP_FEMEI[1] || gender == CNP_FEMEI[2]) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isFemale3() {
        char gender = cnp.charAt(CNP_POZITIE_GEN);
        for (int i = 0; i < CNP_FEMEI.length; i++) {
            if (CNP_FEMEI[i] == gender) {
                return true;
            }
        }
        return false;
    }

    public boolean isMale3() {
        char gender = cnp.charAt(CNP_POZITIE_GEN);
        for (int i = 0; i < CNP_BARBATI.length; i++) {
            if (CNP_BARBATI[i] == gender) {
                return true;
            }
        }
        return false;
    }

    public int getVarsta() {
        return Period.between(LocalDate.now(), dataNasterii).getYears();
    }

}
