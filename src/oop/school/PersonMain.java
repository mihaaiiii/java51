package oop.school;

public class PersonMain {
    public static void main(String[] args) {
        Person p1 = new Person(
                "6211215890123",
                "Pop",
                "Georgiana",
                1500
        );
        double noulSalariu = 1700;
        p1.setSalariul(noulSalariu);
        p1.setSalariul(9000);

        if(p1.isFemale()) {
            System.out.println(p1.getNume() + " " + p1.getPrenume() + " este femeie.");
        } else {
            System.out.println(p1.getNume() + " " + p1.getPrenume() + " este bărbat.");
        }

        System.out.println(p1.getDataNasterii());
        System.out.println(p1.getVarsta());

        // creat persoane
        // modificat salariul să funcționeze
        // modificat salariul nu să funcționeze
        // modificat salariul minim pe economie să funcționeze
        // modificat salariul minim pe economie nu să funcționeze

        // adaugat un field nou: daca persoana e muncitor calificat sau nu

        // creat o lista de persoane

        // afisat pentru fiecare persoana o prezentare (Eu sunt X, sunt muncitor calificat)

        // calculare salariul mediu pentru muncitori calificati si pentru muncitori necalificati



    }
}
