package oop.school;

public class Student {

    static private int pragBursa = 9;
    // int pentru varsta
    private int varsta;
    // String pentru nume
//    protected String nume;  (protected e deafult acces modifier = dacă nu punem niciun acces modifier,
//                              se considera ca am pus "protected")
//    String nume;              liniile 7 si 9 sunt echivalente
    // variabilele/metodele/clasele care sunt declarate cu "protected" sunt vizibile doar în pachetul lor
    public String nume;

    // field sau class variable (variabila de clasa)
    double medieGenerala;

    protected Student() {
    }

    // variabilele/metodele (inclusiv constructorii) /clasele sunt vizibile peste tot in proiect
    public Student(String pNume, int pVarsta, double pMediaGenerala) {
        nume = pNume;
        varsta = pVarsta;
        medieGenerala = pMediaGenerala;
    }

    public String hasScholarship() {
        if (medieGenerala > pragBursa) {
            return nume + " primeste bursa!";
        } else {
            return nume + "nu primeste bursa. E nevoie de cel puțin nota 9.";
        }
    }

    // nume, varsta, mediaGenerala
    public String introduceYourself() {
        return "Salut! Eu sunt " + nume +
                " și am " + varsta +
                " ani iar media mea este " + medieGenerala;
    }

    public static int getPragBursa() {
        return pragBursa;
    }

    protected static void setPragBursa(int noulPrag) {
        pragBursa = noulPrag;
    }

}
