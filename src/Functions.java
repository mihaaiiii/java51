import java.util.Scanner;

public class Functions {
    public static void main(String[] args) {

        printGreatest(12, 33);

        int nr1 = 4, nr2 = -67;
        printGreatest(nr1, nr2);

        Scanner keyboard = new Scanner(System.in);
        System.out.println("Dati doua numere: ");
        int x = keyboard.nextInt();
        int y = keyboard.nextInt();
        printGreatest(x, y);

        int result;
        result = cube(3);
        System.out.println("Main says the cube of 3 is " + result);

        System.out.println("Main says the square of 7 is " + square(7));

        System.out.println("Factorialul lui 5 e " + factorial(5) );
    }

    // static - void <- pentru ca nu returneaza nimic - numele functiei
    static void printGreatest(int x, int y) {
        if (x > y) {
            System.out.println(x + " este mai mare");
        } else if (y > x) {
            System.out.println(y + " este mai mare");
        } else {
            System.out.println(x + " = " + y);
        }
    }

    // static - tipul de date care este returnat - numele functiei - ( - parametrii - )
    static int cube(int nr) {
        int c;
        c = nr * nr * nr;
        return c;
    }

    static int square(int nr) {
        return nr * nr;
    }

    // returnează produsul numerelor până la n
    // factorial(n)
    static int factorial (int nr) {
        int fact = 1;
        for (int i=1; i<= nr; i++) {
            fact = fact * i;
        }
        return fact;
    }
}
