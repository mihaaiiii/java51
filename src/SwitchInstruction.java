public class SwitchInstruction {

    public static void main(String[] args) {

        int zar = 5;
        switch (zar) {
            case 1:
                System.out.println("Ai dat 1. Felicitări, ai câștigat!");
                break;
            case 2:
                System.out.println("2 - Try again");
                break;
            case 3:
                System.out.println("3 - Try again");
                break;
            case 4:
                System.out.println("4 - Try again");
                break;
            case 5:
                System.out.println("5 - Try again");
                break;
            case 6:
                System.out.println("6 - Contragts. You win!!!");
                break;
            default:
                System.out.println("This is not a valid throw!");
        }
        // int, short, byte, char, string
        switch (zar) {
            case 1:
                System.out.println("Ai dat 1. Felicitări, ai câștigat!");
                break;
            case 2:
            case 3:
            case 4:
            case 5:
                System.out.println(zar + " - Try again");
                break;
            case 6:
                System.out.println("6 - Contragts. You win!!!");
                break;
            default:
                System.out.println("This is not a valid throw!");
        }

        String fruct = "piersica";
        switch (fruct) {
            case "mar": 
                System.out.println("Avem mere");
                break;
            case "piersica":
                System.out.println("Avem piersici");
                break;
        }


        if (zar == 1 || zar == 6) {
            System.out.println("6 - Contragts. You win!!!");
        } else if (zar > 1 && zar < 6) {
            System.out.println(zar + " - Try again");
        } else {
            System.out.println("This is not a valid throw!");
        }

        // se da un caracter - sa se afiseze daca e vocala
        // a este vocala
    }
}
