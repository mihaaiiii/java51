import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class DateTypes {

    public static void main(String[] args) {
        LocalTime localTime = LocalTime.now();
        System.out.println(localTime);
        LocalTime increasedLocalTime = localTime.plusMinutes(90);
        System.out.println(increasedLocalTime);
        System.out.println(localTime.format(DateTimeFormatter.ISO_TIME));
        System.out.println(localTime.withMinute(0));
        System.out.println(localTime.withSecond(0));


        LocalDate localDate = LocalDate.now();
        System.out.println(localDate);
        System.out.println(localDate.format(DateTimeFormatter.ISO_WEEK_DATE));
        System.out.println(localDate.getDayOfYear());
        System.out.println(localDate.lengthOfMonth());
        System.out.println(localDate.format(DateTimeFormatter.BASIC_ISO_DATE));

        DateTimeFormatter roFormatter = DateTimeFormatter.ofPattern("dd-MM-YYYY");
        System.out.println(localDate.withDayOfMonth(2).format(roFormatter));

        LocalDate birthday = LocalDate.of(1989, 3, 18);
        System.out.println(birthday.format(roFormatter));

        System.out.println(birthday.compareTo(localDate));





//        LocalTime givenTime = LocalTime.from()
    }
}
