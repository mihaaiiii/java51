import oop.school.Student;

import java.util.Scanner;

public class WhileInstruction {

    public static void main(String[] args) {

        int nr = 4;

        // while condition is true, execute
        while (nr < 10) {
            System.out.println(nr + " ");
            nr = nr + 3;
        }
        // 4 7


        // afișați nr pare de la 0 la 20 (ordine crescătoare)
        nr = 0;
        while (nr <= 20) {
            System.out.println(nr + " ");
            nr += 2;
        }

        // afișați nr impare de la 40 la 20 (ordine descrescătoare)
        nr = 39;
        while (nr >= 21) {
            System.out.println(nr);
            nr--;
        }


        // Selectați 1 pentru română, 2 pentru engleză. For english press 2, for romanian press 1.
        // 1
        // Bun venit!

        // 2
        // Welcome

        // *orice alt număr
        // Selectați 1 pentru română, 2 pentru engleză. For english press 2, for romanian press 1.

        Scanner keyboard = new Scanner(System.in);
        int tasta = 0;
        while (tasta !=1 && tasta != 2) {
            System.out.println("Selectați 1 pentru română, 2 pentru engleză. For english press 2, for romanian press 1.");
            tasta = keyboard.nextInt();
        }
        if(tasta == 1) {
            System.out.println("Bun venit!");
        } else {
            System.out.println("Welcome!");
        }


        int contor = 0;
        int suma = 0;
        nr = keyboard.nextInt();
        while (nr != 0) {
            suma = suma + nr; // adun numărul în sumă
            contor = contor + 1; // contorizez că am citit un număr valid
            nr = keyboard.nextInt(); // citesc următorul număr
        }
        double mediaAritmetică = ((double) suma)/contor;
        System.out.println("Am citit în total " + contor
                + " numere care au suma " + suma
                + " și media artimetică " + mediaAritmetică);

        // produsul numerelor
        // doar suma numerelor pare
        System.out.println("suma numere pare:");
        contor = 0;
        suma = 0;
        nr = keyboard.nextInt();
        while (nr != 0) {
            if(nr % 2 == 0) {
                suma = suma + nr; // adun numărul în sumă
                contor = contor + 1; // contorizez că am citit un număr valid (par)
            }
            nr = keyboard.nextInt(); // citesc următorul număr
        }
        mediaAritmetică = ((double) suma)/contor;
        System.out.println("Am citit în total " + contor
                + " numere care au suma " + suma
                + " și media artimetică " + mediaAritmetică);


    }
}
