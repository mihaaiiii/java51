import java.util.Scanner;

public class Recap {

    public static void main(String[] args) {
        // declarare de variabilă
        // tip de memorie, numele variabilei
        int i;
        float varF; // float poate stoca maxim 7 zecimale după virgulă
        double dd; // poate stoca maxim 16 zecimale după virgulă
        long l;
        char c;
        String sirDeCaractere;
        boolean b;

        // asignare de valoare
        i = 5;
        varF = 2.5f;
        dd = 55.7;

        // declarare si asignare
        int i1 = 13;

        // operații
        i = i + 5; // + - * /
        i = 15 / 4; // se ignoră ce e după virgulă => i = 3;
        i = 19 % 4; // 19 modulo 4 => i = 3 (19 : 4 = 4 rest 3)

        b = true && false; // o propoziție cu ȘI e adevărată doar când toate părțile ei sunt adevărate
        b = true || false; // o propoziție cu SAU e adevărată când cel puțin o parte a ei este adevărată
        b = !false;         // not false

        String s = "Ana are " + "mere"; // concatenare

        // instructiuni decizionale
        i = 17;
        if (i == 17) {
            System.out.println("Condiția e adevărată pe ramura asta");
        }

        if (i != 17) {
            // nu ajunge aici
            System.out.println("Hello");
        } else {
            System.out.println("Condiția e falsă pe ramura asta");
        }

        // restul împărțirii la 2 poate fi doar
        // 0 <- înseamnă că numărul e par
        // 1 <- înseamnă că numărul e impar
        if (i % 2 == 1) {
            System.out.println("Numărul este impar");
        } else {
            System.out.println("Numărul este par");
        }

        // numere negative, numere pozitive și zero
        if (i < 0) {
            System.out.println("Numărul e negativ");
        } else if (i > 0) {
            System.out.println("Numărul e pozitiv");
        } else {
            System.out.println("Numărul e nul");
        }

        // i este în intervalul 13 - 27 și i este divizibil cu 3
        if (i > 13 && i < 27 && i % 3 == 0) {
            System.out.println(" i este în intervalul 13 - 27 și i este divizibil cu 3");
        }

        // i este în intervalul 13 - 27 SAU i este divizibil cu 3
        if (i > 13 && i < 27 || i % 3 == 0) {
            System.out.println(" i este în intervalul 13 - 27 sau i este divizibil cu 3");
        }

        // ? ce afișează pentru i = 9 -> AFIȘEAZĂ TRUE
        // ORDINEA OPERATIILOR CONTEAZA
        // 2 + 3 * 4  = 14
        i = 9;
        if (i > 13 || i % 3 == 0 && i < 27) {
            // prima dată se face && - ul, și doar apoi se face ||-ul
            System.out.println("Condiția scrisă greșit");
        }

        // instructiunea decizionala switch
        c = 'a';
        switch (c) {
            case 'a':
                System.out.println("Valoarea dată e a, prima literă din alfabet.");
                break;
            case 'z':
                System.out.println("Valoarea dată e z, ultima litera din alfabet.");
                break;
            default:
                System.out.println("Am primit caracterul " + c + " dar nu putem afirma nimic despre el");
                break;
        }

        switch (c) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                System.out.println("Este vocala " + c);
                break;
            default:
                System.out.println("Nu este vocală");
                break;
        }

        // instructiuni repetitive
        // instructiunea for "pentru"
        for (i = 0; i <= 9; i++) { // pentru i de la 0 la 9 cu pas de unu execută:
            System.out.print("* ");
        }
        System.out.println("@");
        // * * * * * * * * * * @

        for (int j = 5; j < 10; j = j + 2) {
            System.out.print(j + " ");
        }
        // 5 7 9

//        System.out.println(j + " "); <- invalid pentru că "j" ne este vizibil aici.
//        El e vizibil doar în blocul de cod în care a fost declarat

        int j = 678;
        System.out.println(j + " "); //asta funcționează pentru că e alt j

        // 100, 90, 80, .... 10
        for (j = 100; j >= 10; j = j - 10) {
            System.out.print(j + " ");
        }

        // afisati toate numere divizibile cu 3 ditnre a și z
        int a = 14, z = 25;
        for (i = a; i <= z; i++) {
            if (i%3 == 0) {
                System.out.print(i + " ");
            }
        }

        // cereti de la tastatura 3 numere in felul următor
        // capt inferior al intervalului
        // capat superior al intervalului
        // numar cu care sa se dividă
        // 13 967 25 <- toate numerele intre 13 si 967 care se divid cu 25
        Scanner keyboard = new Scanner(System.in);
        int inf, sup, div;
        System.out.println("Dați capătul inferior al intervalului:");
        inf = keyboard.nextInt();
        System.out.println("Dati capătul superior al intervalului:");
        sup = keyboard.nextInt();
        System.out.println("Dati divizorul numerelor pe care le vreți afișate:");
        div = keyboard.nextInt();
        for(int nr = inf; nr <= sup; nr++) {
            if(nr%div == 0) {
                System.out.print(nr + " ");
            }
        }

        int sum = 0;
        while (sum < 21) { // cât timp condiția e adevărată, execută ce e în blocul de cod
            System.out.println("Dati o carte");
            int carte = keyboard.nextInt();
//            sum = sum + carte;
            sum += carte;
        }
        // ca să ajungem aici cu rulatul codului, "sum" e musai să fie >= cu 21,
        // ca să poată ieși din bucla while de deasupra
        if(sum == 21) {
            System.out.println("Winner!!!");
        } else {
            System.out.println("Bad dealer!!!");
        }

        int nr = inf;
        while(nr <= sup) {

        }


    }
}
