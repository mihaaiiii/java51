import java.util.Locale;

public class PrimitivesAndObjects {

    final static String NUME_MODUL = "Java Fundamentals";

    public static void main(String[] args) {

        final String numărSesiune = "A 5-a sesiune";

        System.out.println(NUME_MODUL + " " + numărSesiune);

        final String valoareaN;
        valoareaN = "prima valoarea";   // nu e schimbarea valorii, ci doar initializarea
//        valoareaN = "a doua valoare";

        int intreg = 4; // default is zero
        double nrZecimal;
        boolean logica; // default is false
        char caracter;

//        intreg.

        String sirDeCaractere;

        sirDeCaractere = "Un sir de caractere...";

        System.out.println(sirDeCaractere);
        System.out.println(sirDeCaractere.toUpperCase(Locale.ENGLISH)); // String is immutable
        System.out.println(sirDeCaractere);
        System.out.println(sirDeCaractere.charAt(3));
        System.out.println(sirDeCaractere.contains("..."));

        // String pool

        sirDeCaractere = sirDeCaractere + "care are continuare";
        System.out.println(sirDeCaractere);

        intreg = intreg + 10;
        System.out.println(intreg);
        intreg = 0;

//        Integer intregObj = new Integer(45);
        Integer intObj = 45;
        System.out.println(intObj.byteValue());
        intObj = null;

        Double doubleObj = 12.4;
        doubleObj = null;



        Boolean boolObj = Boolean.FALSE;
        System.out.println(boolObj);
        boolObj = true;
        System.out.println(boolObj);



    }

}
