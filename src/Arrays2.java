import java.util.Scanner;

public class Arrays2 {

    public static void main(String[] args) {
        // 342 52 132 542 545
        // int n1, n2, n3 ...;
        int[] numere;
        numere = new int[10];
        numere[0] = 19;
        numere[3] = 5;

//        System.out.println(numere[0]);
//        System.out.println(numere[1]);
//        System.out.println(numere[2]);
//        System.out.println(numere[3]);
//        // ...
//        System.out.println(numere[9]);
//        System.out.println(numere[10]); index out of bounds - avem doar 10 pozitii, de la 0 la 9, 10 e in afara limitelor
        for(int i = 0; i<=9; i++) {
            System.out.println("Pe poziția " + i +
                    " e numărul " + numere[i] + " ");
        }

        // numerotarea pozitiei de la 0, 1,    2,  3
        double[] numereZecimale = {3.78, 8, 6.55, 9.12};
        System.out.println(numereZecimale[2]); // afiseaza 6.55, numarul de pe pozitia 2

        Scanner scanner = new Scanner(System.in);
//        numere[0] = scanner.nextInt();
//        numere[1] = scanner.nextInt();
//        numere[2] = scanner.nextInt();
//        numere[3] = scanner.nextInt();
//        numere[4] = scanner.nextInt();
//        ...
//        numere[0] = scanner.nextInt();
        int[] numbers = new int[5];
        int i2;
        for (i2=0; i2<=4; i2++) {
            numbers[i2] = scanner.nextInt();
        }

        int suma = 0;
        System.out.print("Ati introdus numerele: ");
        for (i2=0; i2<=4; i2++) {
            // suma = suma + numbers[i2]
            suma += numbers[i2];
            System.out.print(numbers[i2] + " ");
        }
        System.out.println();
        System.out.println("Suma lor este " + suma);




    }
}
