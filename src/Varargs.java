public class Varargs {

    public static void main(String[] args) {

        double media = mediaNotelor(8, 9, 10, 9, 9);
        System.out.println(media);

    }

    public static double mediaNotelor(int... note) {
        // note e un array int[]
        double suma = 0;
        for (int i = 0; i < note.length; i++) { // 0, 1, 2, 3,...length
            suma = suma + note[i];              // note[i] note[0]
        }
        return suma / note.length;
    }

    // creati o metoda care accepta ca parametri un numar variabil de cnp=uri
    // metoda trebuie sa printeze pentru fiecare cnp daca e valid
    public static void valideazaCnp(String... listaCnp) {
        for (int x = 0; x < listaCnp.length; x++) {
            if (listaCnp[x].matches("[0-9]{13}")) {
                System.out.println(listaCnp[x] + " e valid.");
            } else {
                System.out.println(listaCnp[x] + " nu e valid.");
            }
        }
    }

    public static void valideazaCnp2(String... listaCnp) {
        for (int x = 0; x < listaCnp.length; x++) {
            if (listaCnp[x].matches("[0-9]{13}")) {
                System.out.println("Cnp nr " + (x + 1) + " " + listaCnp[x] + " e valid.");
            } else {
                System.out.println("Cnp nr " + (x + 1) + " " + listaCnp[x] + " nu e valid.");
            }
        }
    }
}
